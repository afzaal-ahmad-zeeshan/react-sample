## React app sample
This basic React.js based app demonstrates how to develop static HTML pages
using React.js and deploy on hosting (or cloud) environments. 

I used [Bootstrap](https://getbootstrap.com/docs/4.0/examples/cover) template
to create the UI in minutes for React app and then build and deploy to Alibaba
Cloud&mdash;you can do the same on other platforms.