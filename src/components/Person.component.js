import React from 'react';

function PersonComponent(props) {
    let person = props.person;
    return (
        <div>
            <p><b>{person.name}</b>
                {
                    person.important ?  
                    <span role="img" aria-label="Important person">
                        &nbsp;&mdash;⭐
                    </span>
                    :
                    <span role="img" aria-label="Not important person">
                        &nbsp;&mdash;☆
                    </span>
                }
                &mdash; {person.age}</p>
            <p>{person.role}</p>
        </div>
    );
}

export default PersonComponent;
