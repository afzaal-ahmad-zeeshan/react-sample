import React, { useState, useEffect } from 'react';
import PersonComponent from './Person.component';

function DataComponent() {
    const [ data, setData ] = useState({ people: null, error: null });

    useEffect(() => {
        console.log('Downloading the data...');
        fetch('https://5879433641669569.ap-southeast-1.fc.aliyuncs.com/2016-08-15/proxy/data-functions/get-all-people/', {
            // Configurations
        }).then(async response => {
            if(response.ok) {
                let content = await response.json();
                setData({ people: content, error: null });
                console.log("Downloaded the data.");
            } else {
                console.log('There was a problem, response object: ', response);
                setData({ people: [], error: true });
            }
        });
    }, []);

    return (
        <div className="text-left">
            {
                data.people == null ? 
                <div>
                    <h4>Loading...</h4>
                    <p>We are loading the data, it might take a minute to two.</p>
                </div>
                : 
                <div>
                    {
                        data.error ?
                        <div>
                            <h4>Oops</h4>
                            <p>Something went wrong during the download of the data.</p>
                        </div>
                        :
                        <div>
                            <h4>People</h4>
                            <div>
                                {
                                    data.people.map(person => {
                                        return <PersonComponent key={person.id} person={person} />
                                    })
                                }
                            </div>
                        </div>
                    }
                </div>
            }
        </div>
    );
}

export default DataComponent;
