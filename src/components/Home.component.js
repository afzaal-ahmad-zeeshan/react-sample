import React from "react";
import logo from "../logo.svg";

function HomeComponent () {
    return (
        <div className="text-left">
            <img src={logo} alt="React.js logo." />
        </div>
    );
}

export default HomeComponent;