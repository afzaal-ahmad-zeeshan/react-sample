import React from "react";

function ContactComponent () {
    return (
        <div className="text-left">
            <h3>Contact Me</h3>
            <p>This template was made available on <a href="https://getbootstrap.com/docs/4.0/examples/cover">https://getbootstrap.com/docs/4.0/examples/cover</a> and I copied the content from there, modified to suit React.js app and published on <a href="https://gitlab.com/afzaal-ahmad-zeeshan/react-sample">GitLab</a> for future use.</p>
            <p>You can reach out to me on Twitter <a href="https://twitter.com/afzaalvirgoboy" target="_blank" rel="noopener noreferrer">@afzaalvirgoboy</a>, 
                or check my online profiles at <a href="http://gitlab.com/afzaal-ahmad-zeeshan/" target="_blank" rel="noopener noreferrer">GitLab</a>,&nbsp;
                <a href="http://github.com/afzaal-ahmad-zeeshan/" target="_blank" rel="noopener noreferrer">GitHub</a> or&nbsp;
                <a href="https://www.linkedin.com/in/afzaalahmadzeeshan/" target="_blank" rel="noopener noreferrer">LinkedIn</a>.</p>
        </div>
    );
}

export default ContactComponent;
