import React from "react";

function FeaturesComponent () {
    return (
        <div className="text-left">
            <h3>Site features</h3>
            <p>Current this site only contains 3 components and demonstrates the basics of static site deployment on Alibaba Cloud OSS&mdash;it can be used for other cloud providers and hosting services.</p>
            <p>I am planning to add more features and demonstrate <b>how to</b> guides using React.js and other services.</p>
            <p>Current stack:</p>
            <ul>
                <li>React.js</li>
                <li>Bootstrap</li>
                <li>React Router</li>
            </ul>
            <p>There are no tests current registered with this project, but I am hoping to add some to show case that side too.</p>
            <p>Psst, this project is available for download from <a href="https://gitlab.com/afzaal-ahmad-zeeshan/react-sample" target="_blank" rel="noopener noreferrer">GitLab</a>.</p>
        </div>
    );
}

export default FeaturesComponent;