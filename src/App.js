import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css';
import HomeComponent from './components/Home.component';
import FeaturesComponent from './components/Features.component';
import ContactComponent from './components/Contact.component';
import DataComponent from './components/Data.component';

function App() {
  return (
    <Router>
      <div className="cover-container d-flex h-100 p-3 mx-auto flex-column">
        <header className="masthead mb-auto">
          <div className="inner">
            <h3 className="masthead-brand">
              <Link to="/" className="title-hyperlink">React Sample</Link>
            </h3>
            <nav className="nav nav-masthead justify-content-center">
              <Link to="/" className="nav-link">Home</Link>
              <Link to="/features" className="nav-link">Features</Link>
              <Link to="/contact" className="nav-link">Contact</Link>
              <Link to="/data" className="nav-link">Data</Link>
            </nav>
          </div>
        </header>

        <main role="main" className="inner cover">
          <Switch>
            <Route exact path="/">
              <HomeComponent />
            </Route>
            <Route path="/features">
              <FeaturesComponent />
            </Route>
            <Route path="/contact">
              <ContactComponent />
            </Route>
            <Route path="/data">
              <DataComponent />
            </Route>
          </Switch>
        </main>

        <footer className="mastfoot mt-auto">
          <div className="inner">
            <p>Template by <a href="https://twitter.com/mdo">@mdo</a>, more details in <Link to="/contact">Contact</Link>.</p>
          </div>
        </footer>
      </div>
    </Router>
  );
}

export default App;
